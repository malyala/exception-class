/*
  Name: Exception.cpp
  Author: Amit Malyala                                     
  Copyright: All rights reserved, Amit Malyala, 2020
  Description: 

  Notes:
  When throwing a exception, avoid memory leaks.
  
  Coding log:
  10-11-20 created initial version.
  
  
  Advantages and disadvantages of creating custom exceptions.
  Disadvantages:
  Any new exception class may not conform with std::exception and may not be portable.
  This new exception class may be redundant compared to already existing std::exception class
  
  Advantages:
  User can Create custom error messages and error types for a project.
  Inability to use stdlib may necessitate the software developer to create new exception types.
  Version and bug history:
  0.1 Initial version.

***********************************************************/

#include <iostream>
#include <cstring>
#include "Exception.h"

except::Exception::Exception(const char* str)
{
	message = new char [std::strlen(str) +1];
	std::strcpy(message,str);
}

except::Exception::Exception()
{
	message=nullptr;
}


except::Exception::~Exception()
{
	if (message)
	{
		delete[] message;
	}
}

const char* except::Exception::GetMessage()
{
	if (message)
	{
		return message;
	}
	else
	{
		return "runtime error";
	}
	
}

// operator << 
std::ostream& operator<<(std::ostream& os,   except::Exception& obj)
{
	os << obj.GetMessage();
    return os;
}

// Sample application
void ExceptionApp(void);

// Function main 
int main(void)
{
	ExceptionApp();
	return 0;
}

// Application
void ExceptionApp(void)
{
	try
	{
		throw except::Exception("This is a self written exception");
	}
	catch ( except::Exception& a)
	{
		std::cout << a.GetMessage();
	}
}
