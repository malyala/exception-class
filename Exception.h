/*
  Name: Exception.h
  Author: Amit Malyala                                     
  Copyright: All rights reserved, Amit Malyala, 2020
  Description: 
  Notes:
  When throwing a exception, avoid memory leaks.
  Coding log:
  10-11-20 created initial version.
  
  Version and bug history:
  0.1 Initial version.

***********************************************************/
#include <ostream>

namespace except
{
   class ExceptBase
   {
   	    protected:
	    char* message;
   	    public:
   	    virtual const char* GetMessage(void) =0;
   	    /* Display output with << operator using Exception object */
	    friend std::ostream& operator<<(std::ostream& os,  ExceptBase& obj);
   };
	
	
	
   class Exception : public ExceptBase
   {
	    
	    public:
	    Exception();
	    ~Exception();
	    Exception(const char* str);
	    virtual const char* GetMessage(void);
	    
   };
}


